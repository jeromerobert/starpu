// StarPU --- Runtime system for heterogeneous multicore architectures.
//
// Copyright (C) 2017       CNRS
// Copyright (C) 2017       Inria
//
// StarPU is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation; either version 2.1 of the License, or (at
// your option) any later version.
//
// StarPU is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the GNU Lesser General Public License in COPYING.LGPL for more details.

knownConditionTrueFalse
variableScope
ConfigurationNotChecked

*:starpufft/*
*:min-dgels/*
*:starpu-top/*
*:socl/src/CL/*

// TODO. We should cppcheck the code
*:sc_hypervisor/*

varFuncNullUB:examples/sched_ctx/two_cpu_contexts.c:76
negativeIndex:examples/stencil/stencil-tasks.c
constStatement:examples/stencil/*

unreadVariable:tests/openmp/*
unusedLabel:tests/datawizard/gpu_register.c
unusedLabel:tests/datawizard/gpu_ptr_register.c
redundantAssignment:tests/datawizard/interfaces/test_interfaces.c:752
redundantAssignment:tests/datawizard/mpi_like_async.c:165
redundantAssignment:tests/datawizard/mpi_like_async.c:211
unusedPrivateFunction:tests/main/combined_workers/bfs/timer.h:45
redundantAssignment:tests/main/driver_api/init_run_deinit.c
redundantAssignment:tests/main/driver_api/run_driver.c
unreadVariable:tests/datawizard/variable_size.c

uselessAssignmentPtrArg:mpi/src/starpu_mpi.c:171
unreadVariable:mpi/src/starpu_mpi.c:971
unusedVariable:mpi/src/starpu_mpi.c:972
redundantAssignment:src/core/workers.c

invalidPointerCast:src/core/perfmodel/perfmodel_nan.c:74
unreadVariable:src/core/dependencies/tags.c:111
uselessAssignmentPtrArg:src/core/sched_ctx_list.c:144
unusedStructMember:src/core/perfmodel/perfmodel_bus.c:62
unusedStructMember:src/core/perfmodel/perfmodel_bus.c:63
unusedStructMember:src/core/perfmodel/perfmodel_bus.c:64
unusedStructMember:src/core/perfmodel/perfmodel_bus.c:65
unusedStructMember:src/core/perfmodel/perfmodel_bus.c:66
unusedStructMember:src/core/simgrid.c:225
unusedStructMember:src/core/simgrid.c:226
wrongPrintfScanfArgNum:src/core/simgrid.c:962
duplicateExpression:src/util/starpu_task_insert.c:52

nullPointerRedundantCheck:src/common/rbtree.c
unreadVariable:src/datawizard/interfaces/*
unreadVariable:src/drivers/driver_common/driver_common.c:493
clarifyCondition:src/drivers/opencl/driver_opencl.c:945
unreadVariable:src/drivers/opencl/driver_opencl.c:767
clarifyCondition:src/drivers/cuda/driver_cuda.c:498
arithOperationsOnVoidPointer:src/drivers/scc/*
nullPointerRedundantCheck:src/sched_policies/deque_modeling_policy_data_aware.c:198
sizeofDereferencedVoidPointer:src/util/fstarpu.c

allocaCalled:gcc-plugin/src/*
unusedVariable:gcc-plugin/tests/*
unreadVariable:gcc-plugin/tests/*
//cppcheck complaints it does not find duplicate expressions
//duplicateExpression:gcc-plugin/src/*
negativeIndex:gcc-plugin/src/*

pointerSize:socl/src/cl_getcontextinfo.c:33
