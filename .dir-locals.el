;; Hey Emacs, use the ugly style!

((c-mode . ((c-file-style . "linux")
	    (c-basic-offset . 8)
	    (tab-width . 8)
	    (indent-tabs-mode . t))))
